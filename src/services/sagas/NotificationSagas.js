import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {GET_NOTIFICATION_LIST_START} from "../constants/constants";
import URL from "../api/config";
import {getNotificationListFail, getNotificationListSuccess} from "../actions";

export function* getNotificationList(action) {
  try {
    const response = yield axios.get(`${URL}/notifications?days=${action.payload ? action.payload : "30"}`);
    yield put(getNotificationListSuccess(response.data));
  } catch (error) {
    yield put(getNotificationListFail(error));
  }
}

export function* getNotificationListStart() {
  yield takeLatest(GET_NOTIFICATION_LIST_START, getNotificationList);
}


export function* notificationSagas() {
  yield all([
    call(getNotificationListStart)
  ]);
}
