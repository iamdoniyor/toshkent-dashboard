import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {
  GET_PROJECT_LIST_START, GET_SINGLE_PROJECT_START,
  POST_CREATE_PROJECT_START
} from "../constants/constants";
import URL from "../api/config";
import {
  getProjectListFail,
  getProjectListSuccess,
  getSingleProjectFail,
  getSingleProjectSuccess,
  postCreateProjectFail,
  postCreateProjectSuccess
} from "../actions";

// GET PROJECT LIST
export function* getProjectList({payload}) {
  try {
    let filterValues = "";
    if (payload) {
      for (let [key, value] of Object.entries(payload)) {
        filterValues += `${key}=${value}&`;
      }
    }

    const response = yield axios.get(`${URL}/projects?${filterValues}`);
    yield put(getProjectListSuccess(response.data));
  } catch (error) {
    yield put(getProjectListFail(error));
  }
}

export function* getProjectListStart() {
  yield takeLatest(GET_PROJECT_LIST_START, getProjectList);
}

// GET SINGLE PROJECT
export function* getSingleProject(action) {
  try {
    const response = yield axios.get(`${URL}/projects/${action.payload}`);
    yield put(getSingleProjectSuccess(response.data));
  } catch (error) {
    yield put(getSingleProjectFail(error));
  }
}

export function* getSingleProjectStart() {
  yield takeLatest(GET_SINGLE_PROJECT_START, getSingleProject);
}

//POST CREATE PROJECT

export function* postCreateProject(action) {
  try {
    const response = yield axios.post(`${URL}/projects`, action.payload);
    yield put(postCreateProjectSuccess(response.data));
  } catch (error) {
    yield put(postCreateProjectFail(error));
  }
}

export function* postCreateProjectStart() {
  yield takeLatest(POST_CREATE_PROJECT_START, postCreateProject);
}


export function* projectSagas() {
  yield all([
    call(getProjectListStart),
    call(postCreateProjectStart),
    call(getSingleProjectStart)
  ]);
}
