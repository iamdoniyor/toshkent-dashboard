import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {
  GET_SINGLE_PROJECT_START,
  POST_CREATE_INVESTMENT_START,
  POST_CREATE_INVESTMENT_SUCCESS,
  POST_CREATE_PROJECT_START
} from "../constants/constants";
import URL from "../api/config";
import {
  getSingleProjectStart,
  postCreateInvestmentFail,
  postCreateInvestmentSuccess, toggleModal
} from "../actions";
import {getSingleProject} from "./ProjectSagas";

//POST CREATE INVESTMENT

export function* postCreateInvestment(action) {
  try {
    const response = yield axios.post(`${URL}/icontracts`, action.payload);

    yield put(getSingleProjectStart(response.data.data.contract.project));
    yield put(toggleModal(false));
    yield put(postCreateInvestmentSuccess(response.data));
  } catch (error) {
    yield put(postCreateInvestmentFail(error));
  }
}

export function* postCreateInvestmentStart() {
  yield takeLatest(POST_CREATE_INVESTMENT_START, postCreateInvestment);
}

// export function* onCreateInvestmentSuccess() {
//   yield takeLatest(POST_CREATE_INVESTMENT_SUCCESS, getSingleProject);
// }


export function* investmentSagas() {
  yield all([
    call(postCreateInvestmentStart)
  ]);
}
