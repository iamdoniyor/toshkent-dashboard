import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {GET_OBLIGATION_LIST_START} from "../constants/constants";
import URL from "../api/config";
import {getObligationListFail, getObligationListSuccess} from "../actions";

export function* getObligationList() {
  try {
    const response = yield axios.get(`${URL}/obligations`);
    yield put(getObligationListSuccess(response.data));
  } catch (error) {
    yield put(getObligationListFail(error));
  }
}

export function* getObligationListStart() {
  yield takeLatest(GET_OBLIGATION_LIST_START, getObligationList);
}


export function* obligationSagas() {
  yield all([
    call(getObligationListStart)
  ]);
}
