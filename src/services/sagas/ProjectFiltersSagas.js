import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {
  GET_NOTIFICATION_LIST_START,
  GET_PROJECT_FILTERS_START
} from "../constants/constants";
import URL from "../api/config";
import {getProjectFiltersFail, getProjectFiltersSuccess} from "../actions";

export function* getProjectFilters() {
  try {
    const response = yield axios.get(`${URL}/projects/filters`);

    yield put(getProjectFiltersSuccess(response.data.data.filters));
  } catch (error) {
    yield put(getProjectFiltersFail(error));
  }
}

export function* getProjectFiltersStart() {
  yield takeLatest(GET_PROJECT_FILTERS_START, getProjectFilters);
}


export function* projectFilterSagas() {
  yield all([
    call(getProjectFiltersStart)
  ]);
}
