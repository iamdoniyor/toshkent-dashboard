import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {GET_STATISTICS_START} from "../constants/constants";
import URL from "../api/config";
import {getStatisticsFail, getStatisticsSuccess} from "../actions";

export function* getStatistics() {
  try {
    const response = yield axios.get(`${URL}/projects/stats`);
    yield put(getStatisticsSuccess(response.data));
  } catch (error) {
    yield put(getStatisticsFail(error));
  }
}

export function* getStatisticsStart() {
  yield takeLatest(GET_STATISTICS_START, getStatistics);
}


export function* statisticsSagas() {
  yield all([
    call(getStatisticsStart)
  ]);
}
