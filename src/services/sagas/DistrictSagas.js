import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {GET_DISTRICT_LIST_START} from "../constants/constants";
import URL from "../api/config";
import {getDistrictListFail, getDistrictListSuccess} from "../actions";

export function* getDistrictList() {
  try {
    const response = yield axios.get(`${URL}/districts`);
    yield put(getDistrictListSuccess(response.data));
  } catch (error) {
    yield put(getDistrictListFail(error));
  }
}

export function* getDistrictListStart() {
  yield takeLatest(GET_DISTRICT_LIST_START, getDistrictList);
}


export function* districtSagas() {
  yield all([
    call(getDistrictListStart)
  ]);
}
