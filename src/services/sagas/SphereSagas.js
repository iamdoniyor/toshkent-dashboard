import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {GET_SPHERE_LIST_START} from "../constants/constants";
import URL from "../api/config";
import {getSphereListFail, getSphereListSuccess} from "../actions";

export function* getSphereList() {
  try {
    const response = yield axios.get(`${URL}/spheres`);
    yield put(getSphereListSuccess(response.data));
  } catch (error) {
    yield put(getSphereListFail(error));
  }
}

export function* getSphereListStart() {
  yield takeLatest(GET_SPHERE_LIST_START, getSphereList);
}


export function* sphereSagas() {
  yield all([
    call(getSphereListStart)
  ]);
}
