import { all, call } from 'redux-saga/effects';

import { projectSagas } from './ProjectSagas';
import {obligationSagas} from "./ObligationSagas";
import {sphereSagas} from "./SphereSagas";
import {districtSagas} from "./DistrictSagas";
import {investmentSagas} from "./InvestmentSagas";
import {socialSagas} from "./SocialSagas";
import {notificationSagas} from "./NotificationSagas";
import {projectFilterSagas} from "./ProjectFiltersSagas";
import {statisticsSagas} from "./StatisticsSagas";

export default function* rootSaga() {
  yield all([
    call(projectSagas),
    call(obligationSagas),
    call(sphereSagas),
    call(districtSagas),
    call(investmentSagas),
    call(socialSagas),
    call(notificationSagas),
    call(projectFilterSagas),
    call(statisticsSagas)
  ])
}
