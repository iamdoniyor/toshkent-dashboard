import { takeLatest, put, all, call } from 'redux-saga/effects';
import axios from 'axios';

import {
  POST_CREATE_SOCIAL_START
} from "../constants/constants";
import URL from "../api/config";
import {
  getSingleProjectStart,
  postCreateSocialFail,
  postCreateSocialSuccess, toggleModal
} from "../actions";

//POST CREATE SOCIAL CONTRACT

export function* postCreateSocial(action) {
  try {

    const response = yield axios.post(`${URL}/scontracts`, action.payload);

    yield put(getSingleProjectStart(response.data.data.contract.project));
    yield put(toggleModal(false));
    yield put(postCreateSocialSuccess(response.data));
  } catch (error) {
    yield put(postCreateSocialFail(error));
  }
}

export function* postCreateSocialStart() {
  yield takeLatest(POST_CREATE_SOCIAL_START, postCreateSocial);
}


export function* socialSagas() {
  yield all([
    call(postCreateSocialStart)
  ]);
}
