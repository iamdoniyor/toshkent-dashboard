import {
  POST_CREATE_SOCIAL_FAIL,
  POST_CREATE_SOCIAL_START,
  POST_CREATE_SOCIAL_SUCCESS
} from "../constants/constants";


// POST CREATE
export const postCreateSocialStart = (data) => ({
  type: POST_CREATE_SOCIAL_START,
  payload: data
});


export const postCreateSocialSuccess = () => {
  return {
    type: POST_CREATE_SOCIAL_SUCCESS
  }};

export const postCreateSocialFail = () => ({
  type: POST_CREATE_SOCIAL_FAIL
});
