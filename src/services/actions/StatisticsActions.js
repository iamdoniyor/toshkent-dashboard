import {
  GET_STATISTICS_FAIL,
  GET_STATISTICS_START,
  GET_STATISTICS_SUCCESS
} from "../constants/constants";

export const getStatisticsStart = () => ({
  type: GET_STATISTICS_START
});


export const getStatisticsSuccess = data => ({
  type: GET_STATISTICS_SUCCESS,
  payload: data
});

export const getStatisticsFail = () => ({
  type: GET_STATISTICS_FAIL
});
