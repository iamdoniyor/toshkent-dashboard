import {TOGGLE_MODAL} from "../constants/constants";

export const toggleModal = status => ({
  type: TOGGLE_MODAL,
  payload: status
});
