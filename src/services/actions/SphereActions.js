import {
  GET_SPHERE_LIST_FAIL,
  GET_SPHERE_LIST_START,
  GET_SPHERE_LIST_SUCCESS
} from "../constants/constants";

export const getSphereListStart = () => ({
  type: GET_SPHERE_LIST_START
});


export const getSphereListSuccess = data => ({
  type: GET_SPHERE_LIST_SUCCESS,
  payload: data
});

export const getSphereListFail = () => ({
  type: GET_SPHERE_LIST_FAIL
});
