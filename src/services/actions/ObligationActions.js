import {
  GET_OBLIGATION_LIST_FAIL,
  GET_OBLIGATION_LIST_START,
  GET_OBLIGATION_LIST_SUCCESS
} from "../constants/constants";

export const getObligationListStart = () => ({
  type: GET_OBLIGATION_LIST_START
});


export const getObligationListSuccess = data => ({
  type: GET_OBLIGATION_LIST_SUCCESS,
  payload: data
});

export const getObligationListFail = () => ({
  type: GET_OBLIGATION_LIST_FAIL
});
