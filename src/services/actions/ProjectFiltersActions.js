import {
  CHANGE_NOTIFICATION_PERIOD,
  GET_PROJECT_FILTERS_FAIL,
  GET_PROJECT_FILTERS_START,
  GET_PROJECT_FILTERS_SUCCESS
} from "../constants/constants";

export const getProjectFiltersStart = () => ({
  type: GET_PROJECT_FILTERS_START
});


export const getProjectFiltersSuccess = data => ({
  type: GET_PROJECT_FILTERS_SUCCESS,
  payload: data
});

export const getProjectFiltersFail = () => ({
  type: GET_PROJECT_FILTERS_FAIL
});
