import {
  POST_CREATE_INVESTMENT_FAIL,
  POST_CREATE_INVESTMENT_START,
  POST_CREATE_INVESTMENT_SUCCESS
} from "../constants/constants";


// POST CREATE
export const postCreateInvestmentStart = (data) => ({
  type: POST_CREATE_INVESTMENT_START,
  payload: data
});


export const postCreateInvestmentSuccess = () => {
  return {
    type: POST_CREATE_INVESTMENT_SUCCESS
  }};

export const postCreateInvestmentFail = () => ({
  type: POST_CREATE_INVESTMENT_FAIL
});
