import {
  CHANGE_NOTIFICATION_PERIOD,
  GET_NOTIFICATION_LIST_FAIL,
  GET_NOTIFICATION_LIST_START,
  GET_NOTIFICATION_LIST_SUCCESS
} from "../constants/constants";

export const getNotificationListStart = (days) => ({
  type: GET_NOTIFICATION_LIST_START,
  payload: days
});


export const getNotificationListSuccess = data => ({
  type: GET_NOTIFICATION_LIST_SUCCESS,
  payload: data
});

export const getNotificationListFail = () => ({
  type: GET_NOTIFICATION_LIST_FAIL
});


export const changeNotificationPeriod = days => ({
  type: CHANGE_NOTIFICATION_PERIOD,
  payload: days
});
