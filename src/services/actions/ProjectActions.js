import {
  CLEAR_SINGLE_PROJECT,
  GET_PROJECT_LIST_FAIL,
  GET_PROJECT_LIST_START,
  GET_PROJECT_LIST_SUCCESS, GET_SINGLE_PROJECT_FAIL,
  GET_SINGLE_PROJECT_START,
  GET_SINGLE_PROJECT_SUCCESS,
  POST_CREATE_PROJECT_FAIL,
  POST_CREATE_PROJECT_START,
  POST_CREATE_PROJECT_SUCCESS
} from "../constants/constants";
import history from "../history";
import {logger} from "redux-logger/src";



// GET LIST
export const getProjectListStart = filters => ({
  type: GET_PROJECT_LIST_START,
  payload: filters
});


export const getProjectListSuccess = data => ({
  type: GET_PROJECT_LIST_SUCCESS,
  payload: data
});

export const getProjectListFail = () => ({
  type: GET_PROJECT_LIST_FAIL
});

// GET SINGLE
export const getSingleProjectStart = (data) => {

  return {
  type: GET_SINGLE_PROJECT_START,
  payload: data
}};


export const getSingleProjectSuccess = data => ({
  type: GET_SINGLE_PROJECT_SUCCESS,
  payload: data
});

export const getSingleProjectFail = () => ({
  type: GET_SINGLE_PROJECT_FAIL
});

// CLEAR SINGLE PROJECT

export const clearSingleProject = () => ({
  type: CLEAR_SINGLE_PROJECT
});

// POST CREATE

export const postCreateProjectStart = (data) => ({
  type: POST_CREATE_PROJECT_START,
  payload: data
});


export const postCreateProjectSuccess = () => {
  history.push("/");
  return {
    type: POST_CREATE_PROJECT_SUCCESS
}};

export const postCreateProjectFail = () => ({
  type: POST_CREATE_PROJECT_FAIL
});
