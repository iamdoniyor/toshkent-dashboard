import {
  GET_DISTRICT_LIST_FAIL,
  GET_DISTRICT_LIST_START,
  GET_DISTRICT_LIST_SUCCESS
} from "../constants/constants";

export const getDistrictListStart = () => ({
  type: GET_DISTRICT_LIST_START
});


export const getDistrictListSuccess = data => ({
  type: GET_DISTRICT_LIST_SUCCESS,
  payload: data
});

export const getDistrictListFail = () => ({
  type: GET_DISTRICT_LIST_FAIL
});
