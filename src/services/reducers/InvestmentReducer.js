import {
  POST_CREATE_INVESTMENT_FAIL,
  POST_CREATE_INVESTMENT_START,
  POST_CREATE_INVESTMENT_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  isCreating: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // POST CREATE
    case POST_CREATE_INVESTMENT_START:
      return {
        ...state,
        isCreating: true
      };
    case POST_CREATE_INVESTMENT_SUCCESS:
      return {
        ...state,
        isCreating: false
      };
    case POST_CREATE_INVESTMENT_FAIL:
      return {
        ...state,
        isCreating: false
      };
    default:
      return state;
  }
};
