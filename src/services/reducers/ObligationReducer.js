import {
  GET_OBLIGATION_LIST_FAIL,
  GET_OBLIGATION_LIST_START,
  GET_OBLIGATION_LIST_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  obligations: [],
  obligationsLoading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_OBLIGATION_LIST_START:
      return {
        ...state,
        obligationsLoading: true
      };
    case GET_OBLIGATION_LIST_SUCCESS:
      return {
        ...state,
        obligationsLoading: false,
        obligations: action.payload.data.obligations
      };
    case GET_OBLIGATION_LIST_FAIL:
      return {
        ...INITIAL_STATE,
        obligationsLoading: false
      };
    default:
      return state;
  }
};
