import {
  GET_STATISTICS_FAIL,
  GET_STATISTICS_START,
  GET_STATISTICS_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  statistics: [],
  statisticsLoading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_STATISTICS_START:
      return {
        ...state,
        statisticsLoading: true
      };
    case GET_STATISTICS_SUCCESS:
      return {
        ...state,
        statisticsLoading: false,
        statistics: action.payload.data.stats
      };
    case GET_STATISTICS_FAIL:
      return {
        ...INITIAL_STATE,
        statisticsLoading: false
      };
    default:
      return state;
  }
};
