import {
  GET_PROJECT_LIST_START,
  GET_PROJECT_LIST_SUCCESS,
  GET_PROJECT_LIST_FAIL,
  POST_CREATE_PROJECT_START,
  POST_CREATE_PROJECT_SUCCESS,
  POST_CREATE_PROJECT_FAIL,
  GET_SINGLE_PROJECT_START,
  GET_SINGLE_PROJECT_SUCCESS,
  GET_SINGLE_PROJECT_FAIL, CLEAR_SINGLE_PROJECT
} from "../constants/constants";

const INITIAL_STATE = {
  projects: [],
  projectsLoading: false,
  isCreating: false,
  project: null,
  projectLoading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // GET LIST
    case GET_PROJECT_LIST_START:
      return {
        ...state,
        projectsLoading: true
      };
    case GET_PROJECT_LIST_SUCCESS:
      return {
        ...state,
        projectsLoading: false,
        projects: action.payload.data
      };
    case GET_PROJECT_LIST_FAIL:
      return {
        ...INITIAL_STATE,
        projectsLoading: false
      };
    // POST CREATE
    case POST_CREATE_PROJECT_START:
      return {
        ...state,
        isCreating: true
      };
    case POST_CREATE_PROJECT_SUCCESS:
      return {
        ...state,
        isCreating: false
      };
    case POST_CREATE_PROJECT_FAIL:
      return {
        ...state,
        isCreating: false
      };
    // GET SINGLE
    case GET_SINGLE_PROJECT_START:
      return {
        ...state,
        projectLoading: true,
        project: null
      };
    case GET_SINGLE_PROJECT_SUCCESS:
      return {
        ...state,
        projectLoading: false,
        project: action.payload.data
      };
    case GET_SINGLE_PROJECT_FAIL:
      return {
        ...state,
        projectLoading: false
      };
      // CLEAR SINGLE PROJECT
      case CLEAR_SINGLE_PROJECT:
      return {
        ...state,
        project: null
      };
    default:
      return state;
  }
};
