import {combineReducers} from "redux";
import { reducer as formReducer } from 'redux-form'
import storage from 'redux-persist/lib/storage'

import {CLEAR_ON_SIGNOUT} from "../constants/constants";

import ProjectReducer from "./ProjectReducer";
import ObligationReducer from "./ObligationReducer";
import SphereReducer from "./SphereReducer";
import DistrictReducer from "./DistrictReducer";
import ModalReducer from "./ModalReducer";
import InvestmentReducer from "./InvestmentReducer";
import SocialReducer from "./SocialReducer";
import NotificationReducer from "./NotificationReducer";
import ProjectFiltersReducer from "./ProjectFiltersReducer";
import StatisticsReducer from "./StatisticsReducer";

const appReducer  =  combineReducers({
  project: ProjectReducer,
  obligation: ObligationReducer,
  sphere: SphereReducer,
  district: DistrictReducer,
  modal: ModalReducer,
  investment: InvestmentReducer,
  social: SocialReducer,
  notification: NotificationReducer,
  projectFilters: ProjectFiltersReducer,
  statistics: StatisticsReducer,
  form: formReducer
});


const rootReducer = (state, action) => {
  if (action.type === CLEAR_ON_SIGNOUT) {
    storage.removeItem('persist:root');

    state = undefined;
  }
  return appReducer(state, action);
};


export default rootReducer
