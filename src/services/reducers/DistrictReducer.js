import {
  GET_DISTRICT_LIST_FAIL,
  GET_DISTRICT_LIST_START,
  GET_DISTRICT_LIST_SUCCESS, POST_CREATE_PROJECT_FAIL,
  POST_CREATE_PROJECT_START,
  POST_CREATE_PROJECT_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  districts: [],
  districtsLoading: false,
  isCreating: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_DISTRICT_LIST_START:
      return {
        ...state,
        districtsLoading: true
      };
    case GET_DISTRICT_LIST_SUCCESS:
      return {
        ...state,
        districtsLoading: false,
        districts: action.payload.data.districts
      };
    case GET_DISTRICT_LIST_FAIL:
      return {
        ...INITIAL_STATE,
        districtsLoading: false
      };
      case POST_CREATE_PROJECT_START:
      return {
        ...state,
        isCreating: true
      };
    case POST_CREATE_PROJECT_SUCCESS:
      return {
        ...state,
        isCreating: false
      };
    case POST_CREATE_PROJECT_FAIL:
      return {
        ...state,
        isCreating: false
      };
    default:
      return state;
  }
};
