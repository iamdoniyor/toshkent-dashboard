import {
  POST_CREATE_SOCIAL_FAIL,
  POST_CREATE_SOCIAL_START,
  POST_CREATE_SOCIAL_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  isCreating: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // POST CREATE
    case POST_CREATE_SOCIAL_START:
      return {
        ...state,
        isCreating: true
      };
    case POST_CREATE_SOCIAL_SUCCESS:
      return {
        ...state,
        isCreating: false
      };
    case POST_CREATE_SOCIAL_FAIL:
      return {
        ...state,
        isCreating: false
      };
    default:
      return state;
  }
};
