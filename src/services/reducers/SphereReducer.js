import {
  GET_SPHERE_LIST_FAIL,
  GET_SPHERE_LIST_START,
  GET_SPHERE_LIST_SUCCESS, POST_CREATE_PROJECT_FAIL,
  POST_CREATE_PROJECT_START,
  POST_CREATE_PROJECT_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  spheres: [],
  spheresLoading: false,
  isCreating: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SPHERE_LIST_START:
      return {
        ...state,
        spheresLoading: true
      };
    case GET_SPHERE_LIST_SUCCESS:
      return {
        ...state,
        spheresLoading: false,
        spheres: action.payload.data.spheres
      };
    case GET_SPHERE_LIST_FAIL:
      return {
        ...INITIAL_STATE,
        spheresLoading: false
      };
    default:
      return state;
  }
};
