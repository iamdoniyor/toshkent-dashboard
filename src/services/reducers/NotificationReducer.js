import {
  CHANGE_NOTIFICATION_PERIOD,
  GET_NOTIFICATION_LIST_FAIL,
  GET_NOTIFICATION_LIST_START,
  GET_NOTIFICATION_LIST_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  notifications: [],
  notificationsLoading: false,
  period: ""
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_NOTIFICATION_LIST_START:
      return {
        ...state,
        notificationsLoading: true
      };
      case CHANGE_NOTIFICATION_PERIOD:
      return {
        ...state,
        period: action.payload
      };
    case GET_NOTIFICATION_LIST_SUCCESS:
      return {
        ...state,
        notificationsLoading: false,
        notifications: action.payload.data
      };
    case GET_NOTIFICATION_LIST_FAIL:
      return {
        ...INITIAL_STATE,
        notificationsLoading: false
      };
    default:
      return state;
  }
};
