import {
  GET_PROJECT_FILTERS_FAIL,
  GET_PROJECT_FILTERS_START,
  GET_PROJECT_FILTERS_SUCCESS
} from "../constants/constants";


const INITIAL_STATE = {
  filters: [],
  filtersLoading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROJECT_FILTERS_START:
      return {
        ...state,
        filtersLoading: true
      };
    case GET_PROJECT_FILTERS_SUCCESS:
      return {
        ...state,
        filtersLoading: false,
        filters: action.payload
      };
    case GET_PROJECT_FILTERS_FAIL:
      return {
        ...INITIAL_STATE,
        filtersLoading: false
      };
    default:
      return state;
  }
};
