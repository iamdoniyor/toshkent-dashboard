import storage from "redux-persist/lib/storage";
import createSagaMiddleware from "redux-saga";
import {persistReducer, persistStore} from "redux-persist";
import logger from "redux-logger";
import {applyMiddleware, createStore} from "redux";

import rootReducer from "./reducers";
import rootSaga from "./sagas";


const persistConfig = {
  key: 'root',
  blacklist: ['form', 'modal'],
  storage,
};
const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [sagaMiddleware];

if (process.env.NODE_ENV === 'development') {
  // middlewares.push(logger);
}


export const store = createStore(persistedReducer, {}, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
