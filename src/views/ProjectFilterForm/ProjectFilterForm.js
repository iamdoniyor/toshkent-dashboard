import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Field, formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {
  getDistrictListStart,
  getObligationListStart, getProjectFiltersStart, getProjectListStart,
  getSphereListStart
} from "../../services/actions";
import SelectInputComp from "../../components/SelectInputComp/SelectInputComp";
import "./styles.css"
import DatePickerComp from "../../components/DatePicker/DatePicker";
import {Button, FormGroup, ListGroupItem} from "shards-react";
import {reset} from 'redux-form';
import FormCheckboxComp
  from "../../components/FormCheckboxComp/FormCheckboxComp";

class ProjectFilterForm extends Component {
  componentDidMount() {
    const { getProjectFiltersStart } = this.props;
    getProjectFiltersStart()
  }

  clearForm = () => {
    const { clearFilters, getProjectListStart } = this.props;
    clearFilters();
    getProjectListStart()
  };

  render() {
    const { clearFilters, projectFilters, projectFiltersLoading, sphereValue, obligationValue, projectsLoading, handleSubmit } = this.props;
    let sectorList = projectFilters.sphere && sphereValue && projectFilters.sphere.find(i => i.name === sphereValue) && projectFilters.sphere.find(i => i.name === sphereValue).sectors && projectFilters.sphere.find(i => i.name === sphereValue).sectors.length ? projectFilters.sphere.find(i => i.name === sphereValue).sectors : null;
    sectorList = sectorList && sectorList.filter(obl => !!obl).map(item => { return  item ? {name: item} : null});
    const sectorListDisabled = sectorList ? !sectorList.length : true;

    let subObligationList = projectFilters.obligation && obligationValue && projectFilters.obligation.find(i => i.name === obligationValue) && projectFilters.obligation.find(i => i.name === obligationValue).subcategory && projectFilters.obligation.find(i => i.name === obligationValue).subcategory.length ? projectFilters.obligation.find(i => i.name === obligationValue).subcategory : null;
    subObligationList = subObligationList && subObligationList.filter(obl => !!obl).map(item => { return  item ? {name: item} : null});
    const subObligationListDisabled = subObligationList ? !subObligationList.length : true;

    return (
      <div>
        <div className="filter-container">
          <div className="mr-4">
            <Field
              name="district"
              validationDisabled
              component={SelectInputComp}
              title={"Tuman"}
              loading={projectFiltersLoading}
              options={projectFilters && projectFilters.district}
            />
          </div>
          <div className="mr-4">
            <Field
              name="sphere"
              validationDisabled
              component={SelectInputComp}
              title={"Soha"}
              loading={projectFiltersLoading}
              options={projectFilters && projectFilters.sphere}
            />
          </div>
          <div className="mr-4">
            <Field
              name="sector"
              validationDisabled
              component={SelectInputComp}
              title={"Tarmoq"}
              loading={projectFiltersLoading}
              inputDisabled={sectorListDisabled}
              options={sectorList && sectorList}
            />
          </div>
          <div className="mr-4">
            <Field
              name="social_obligation"
              validationDisabled
              component={SelectInputComp}
              title={"Ijtimoiy majburiyat"}
              loading={projectFiltersLoading}
              options={projectFilters && projectFilters.obligation}
            />
          </div>
          <div className="mr-4">
            <Field
              name="social_sub_obligation"
              validationDisabled
              component={SelectInputComp}
              title={"Ijtimoiy majburiyat (qo'sh)"}
              loading={projectFiltersLoading}
              inputDisabled={subObligationListDisabled}
              options={subObligationList && subObligationList}
            />
          </div>
        </div>
        <br/>
        <div className="filter-container">
          <div className="mr-4">
            <Field
              name="social_end"
              validationDisabled
              component={DatePickerComp}
              title={"Ijtimoiy majburiyat muddati"}
            />
          </div>
          <div className="mr-4">
            <Field
              name="investment_end"
              validationDisabled
              component={DatePickerComp}
              title={"Investitsiya muddati"}
            />
          </div>
          <div className="mr-4">
            <Field
              name="completedFrom"
              component={DatePickerComp}
              validationDisabled
                title={"Tugallangan sana boshi"}
            />
          </div>
          <div className="mr-4">
            <Field
              name="completedTo"
              validationDisabled
              component={DatePickerComp}
              title={"Tugallangan sana oxiri"}
            />
          </div>
          <div className="mr-4">
            <label >Status</label>
            <div className="checkbox-container pt-1">
              <Field
                name="completed"
                title="Tugatilgan"
                component={FormCheckboxComp}
              />
            </div>
          </div>
        </div>
        <div className="mb-4">
          <Button className="py-3 px-5 mr-4"  outline={projectsLoading} disabled={projectsLoading} onClick={(props) => !projectsLoading ? handleSubmit(props) : null}>Saqlash</Button>
           <Button className="py-3 px-5" theme="secondary"  onClick={() =>  this.clearForm()}>Barchasi</Button>
        </div>
      </div>

    );
  }
}

const selector = formValueSelector('ProjectFilterForm');
const AppScreen = connect(
  (state) => ({
    obligationValue: selector(state, 'social_obligation'),
    sphereValue: selector(state, 'sphere'),
    projectsLoading: state.project.projectsLoading,
    projectFilters: state.projectFilters.filters,
    projectFiltersLoading: state.projectFilters.filtersLoading
  }),
  (dispatch) => ({
    getProjectFiltersStart: () => dispatch(getProjectFiltersStart()),
    clearFilters: () => dispatch(reset("ProjectFilterForm")),
    getProjectListStart: (filters) => dispatch(getProjectListStart(filters))
  }))(reduxForm({
  // a unique name for the form
  form: 'ProjectFilterForm',
  touchOnChange: true,
  fields: [
    'district',
    'sphere',
    'sector',
    'social_obligation',
    'social_sub_obligation',

    'social_end',
    'investment_end',
    'completedFrom',
    'completedTo',
    'completed'
  ],
  // validate
})(ProjectFilterForm));

export default AppScreen;
