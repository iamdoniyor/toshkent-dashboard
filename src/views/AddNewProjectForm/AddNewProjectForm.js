import React, {Component} from 'react';
import {
  Button,
  Card,
  Col,
  Container,
  ListGroupItem,
  Row
} from "shards-react";
import {Field, formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";


import PageTitle from "../../components/common/PageTitle";

import CustomFormInput from "../../components/FormInput/FormInput";
import DatePickerComp from "../../components/DatePicker/DatePicker";
import SelectInputComp from "../../components/SelectInputComp/SelectInputComp";
import {
  getDistrictListStart,
  getObligationListStart,
  getSphereListStart
} from "../../services/actions";

class AddNewProjectForm extends Component {

  componentDidMount() {
    const {
      getObligationListStart,
      getSphereListStart,
      getDistrictListStart} = this.props;
    getObligationListStart();
    getSphereListStart();
    getDistrictListStart()
  }

  render() {
    const {
      handleSubmit,
      obligations,
      obligationsLoading,
      obligationValue,
      spheres,
      spheresLoading,
      sphereValue,
      isCreating,
      districts,
      districtsLoading} = this.props;
    const subObligations = obligations && obligationValue && obligations.find(i => i.name === obligationValue) && obligations.find(i => i.name === obligationValue).subcategories && obligations.find(i => i.name === obligationValue).subcategories.length ? obligations.find(i => i.name === obligationValue).subcategories : null;
    const sectors = spheres && sphereValue && spheres.find(i => i.name === sphereValue) && spheres.find(i => i.name === sphereValue).sectors && spheres.find(i => i.name === sphereValue).sectors.length ? spheres.find(i => i.name === sphereValue).sectors : null;
    return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle
            sm="4"
            title="Yangi Loyiha Qo'shish"
            className="text-sm-left"
          />
        </Row>

        <Row>
          <Col lg={{ size: 8, order: 2, offset: 2 }}
               // lg="6"
               className="mb-4">
            <Card small>
              <ListGroupItem className="p-3">
                <Row>
                  <Col>
                    <Field
                      name="number"
                      component={CustomFormInput}
                      title={"Loyiha raqami"}
                    />
                    <Field
                      name="protocolNumber"
                      component={CustomFormInput}
                      title={"Protokol raqami"}
                    />
                    <Field
                      name="name"
                      component={CustomFormInput}
                      title={"Loyiha nomi"}
                    />
                    <Field
                      name="companyName"
                      component={CustomFormInput}
                      title={"Kompaniya nomi"}
                    />
                      <Field
                        name="district"
                        component={SelectInputComp}
                        title={"Tuman"}
                        loading={districtsLoading}
                        options={districts && districts}
                      />
                      <Field
                        name="sphere"
                        component={SelectInputComp}
                        title={"Soha"}
                        loading={spheresLoading}
                        options={spheres && spheres}
                      />
                    <Field
                      name="sector"
                      component={SelectInputComp}
                      title={"Tarmoq"}
                      loading={spheresLoading}
                      options={sectors && sectors}
                    />
                      <Field
                        name="social_obligation"
                        component={SelectInputComp}
                        title={"Ijtimoiy majburiyat turi"}
                        loading={obligationsLoading}
                        options={obligations && obligations}
                      />
                    { subObligations ?
                    <Field
                      name="social_sub_obligation"
                      component={SelectInputComp}
                      title={"Ijtimoiy majburiyat turi (qo'shimcha)"}
                      loading={obligationsLoading}
                      options={subObligations}
                    /> : null}
                    <Field
                      name="social_end"
                      component={DatePickerComp}
                      title={"Ijtimoiy majburiyat muddati"}
                    />
                    <Field
                      name="investment_end"
                      component={DatePickerComp}
                      title={"Investitsiya majburiyati muddati"}
                    />
                    <Field
                      name="decision"
                      component={CustomFormInput}
                      title={"Hokim qarori"}
                    />
                  </Col>
                </Row>
                <Button outline={isCreating} disabled={isCreating} onClick={(props) => !isCreating ? handleSubmit(props) : null}>{isCreating ? "Saqlanmoqda... " : "Saqlash"}</Button>
              </ListGroupItem>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

const validate = formValues => {
  const errors = {};

  if (!formValues.name) {
    errors.name = "Loyiha nomini kiriting"
  }

  if (!formValues.number) {
    errors.number = "Loyiha raqamini kiriting"
  }

  if (!formValues.companyName) {
    errors.companyName = "Kompaniya nomini kiriting"
  }

  if (!formValues.sphere) {
    errors.sphere = "Sohani tanlang"
  }

  if (!formValues.district) {
    errors.district = "Tumanni tanlang"
  }

  if (!formValues.sector) {
    errors.sector = "Tarmoqni tanlang"
  }

  if (!formValues.social_obligation) {
    errors.social_obligation = "Ijtimoiy majburiyatni tanlang"
  }

  if (!formValues.social_end) {
    errors.social_end = "Ijtimoiy majburiyat muddatini kiting"
  }

  if (!formValues.investment_end) {
    errors.investment_end = "Investitsiya majburiyati muddatini kiting"
  }

  if (!formValues.protocolNumber) {
    errors.protocolNumber = "Protokol raqamini kiting"
  }

  return errors;
};


const selector = formValueSelector('ProjectForm');
const AppScreen = connect(
  (state) => ({
    obligations: state.obligation.obligations,
    obligationsLoading: state.obligation.obligationsLoading,
    obligationValue: selector(state, 'social_obligation'),
    spheres: state.sphere.spheres,
    spheresLoading: state.sphere.spheresLoading,
    sphereValue: selector(state, 'sphere'),
    districts: state.district.districts,
    districtsLoading: state.district.districtsLoading,
    isCreating: state.project.isCreating,
  }),
  (dispatch) => ({
    getObligationListStart: () => dispatch(getObligationListStart()),
    getSphereListStart: () => dispatch(getSphereListStart()),
    getDistrictListStart: () => dispatch(getDistrictListStart())
  }))(reduxForm({
  // a unique name for the form
  form: 'ProjectForm',
  touchOnChange: true,
  fields: [
    'number',
    'name',
    'district',
    'companyName',
    'sphere',
    'sector',
    'social_obligation',
    'social_sub_obligation',
    'social_end',
    'investment_end',
    'protocolNumber',
    'decision'
  ],
  validate
})(AddNewProjectForm));

export default AppScreen;
