import React, {Component} from 'react';
import PieChartComp from "../../components/PieChartComp/PieChartComp";
import {connect} from "react-redux";
import {
  clearSingleProject,
  getSingleProjectStart, getStatisticsStart,
  postCreateInvestmentStart, postCreateSocialStart, toggleModal
} from "../../services/actions";
import PageLoader from "../../components/PageLoader/PageLoader";
import {Col, Container, Row} from "shards-react";
import PageTitle from "../../components/common/PageTitle";
import Roadmap from "../../components/Roadmap/Roadmap";

class StatisticsPage extends Component {

  componentDidMount() {
    const { getStatisticsStart } = this.props;
    getStatisticsStart()
  }

  render() {
    const { statistics, statisticsLoading} = this.props;

    // if (statisticsLoading) {
    //   return <PageLoader/>
    // }

    return (
      <Container fluid className="main-content-container px-4">
        <Row>
          <Row noGutters className="page-header py-4">
            <PageTitle title="Statistika" subtitle="Loyihalar" className="text-sm-left col-sm-12" />
          </Row>
        </Row>
        <Row className="mb-4">
        <Col lg={{size:6}}>
        {(statistics && statistics.sphere) ?
        <PieChartComp title={"Sohalar"} chartDataArray={statistics.sphere ? statistics.sphere : []}/> : null}
        </Col>
          <Col lg={{size:6}}>
        {(statistics && statistics.sector) ?
        <PieChartComp title={"Tarmoqlar"} chartDataArray={statistics.sector ? statistics.sector.map(item => ({name: `${item.name.sphere}: ${item.name.sector}`, num: item.num})) : []}/> : null}
        </Col>
      </Row>
        <Row>
          <Col lg={{ size: 8, order: 2, offset: 2 }}
               className="mb-4">
            {(statistics && statistics.obligation) ?
              <PieChartComp title={"Ijtimoiy Majburiyatlar"} chartDataArray={statistics.obligation ? statistics.obligation : []}/> : null} </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  (state) => ({
    statistics: state.statistics.statistics,
    statisticsLoading: state.statistics.statisticsLoading
  }),
  (dispatch) => ({
    getStatisticsStart: () => dispatch(getStatisticsStart()),
  })
)(StatisticsPage);
