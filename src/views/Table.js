import React from "react";
import {Card, CardHeader, CardBody, Button, Row} from "shards-react";

const Table = ({theme, header, body, title, button, buttonTitle, func}) => (
  <Card small className="mb-4">
    <CardHeader className={`${(theme === "dark") ? 'bg-dark' : ''} border-bottom`}>
      <h6 className={`${(theme === "dark") ? 'text-white' : ''} m-0`}>{title}</h6>
      {button ?
      <Button className="my-2" onClick={func}>{buttonTitle}</Button>
        : null}
    </CardHeader>
    <CardBody style={{overflowX: "scroll"}} className={`${(theme === "dark") ? 'bg-dark' : ''} p-0 pb-3`}>
      <table className={`${(theme === "dark") ? 'table-dark' : ''} table mb-0`}>
        <thead className={`${(theme === "dark") ? 'thead-dark' : 'bg-light'}`}>
        <tr>
          {header && header.map((item, i) => (<th key={i} scope="col" className="border-0">{item}</th>))}
        </tr>
        </thead>
        <tbody>
        {body && body}

        </tbody>
      </table>
    </CardBody>
  </Card>
);

export default Table;
