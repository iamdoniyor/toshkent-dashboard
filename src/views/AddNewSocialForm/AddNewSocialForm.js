import React, {Component} from 'react';
import {Field, formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {
  getObligationListStart
} from "../../services/actions";
import CustomFormInput from "../../components/FormInput/FormInput";
import Wrapper from "../../containers/Wrapper";
import DatePickerComp from "../../components/DatePicker/DatePicker";
import {Button} from "shards-react";
import SelectInputComp from "../../components/SelectInputComp/SelectInputComp";


class AddNewSocialForm extends Component {
  componentDidMount() {
    const {
      getObligationListStart} = this.props;
    getObligationListStart();
  }

  render() {
    const {isCreating, handleSubmit, obligationsLoading, obligations, obligationValue} = this.props;
    const subObligations = obligations && obligationValue && obligations.find(i => i.name === obligationValue) && obligations.find(i => i.name === obligationValue).subcategories && obligations.find(i => i.name === obligationValue).subcategories.length ? obligations.find(i => i.name === obligationValue).subcategories : null;

    return (
      <Wrapper>
        <Field
          name="obligation"
          component={SelectInputComp}
            title={"Majburiyat turi"}
          loading={obligationsLoading}
          options={obligations && obligations}
        />
        { subObligations ?
          <Field
            name="sub_obligation"
            component={SelectInputComp}
            title={"Ijtimoiy majburiyat turi (qo'shimcha)"}
            loading={obligationsLoading}
            options={subObligations}
          /> : null}
        <Field
          name="comment"
          component={CustomFormInput}
          title={"Izoh"}
        />
        <Field
          name="unit"
          component={CustomFormInput}
          title={"Birlik"}
        />
        <Field
          name="amount"
          component={CustomFormInput}
          title={"Qiymat"}
          number
        />
        <Field
          name="date"
          component={DatePickerComp}
          title={"Muddati"}
        />
        <Button outline={isCreating} disabled={isCreating} onClick={(props) => !isCreating ? handleSubmit(props) : null}>{isCreating ? "Saqlanmoqda... " : "Saqlash"}</Button>
      </Wrapper>
    );
  }
}


const validate = formValues => {
  const errors = {};

  if (!formValues.obligation) {
    errors.obligation = "Ijtimoiy majburiyat turini kiriting"
  }

  if (!formValues.unit) {
    errors.unit = "Birlikni kiriting"
  }

  if (!formValues.amount) {
    errors.amount = "Ijtimoiy majburiyat qiymatini kiriting"
  }

  if (!formValues.date) {
    errors.date = "Ijtimoiy majburiyat muddatini tanlang"
  }

  if (!formValues.date) {
    errors.date = "Ijtimoiy majburiyat muddatini tanlang"
  }



  return errors;
};

const selector = formValueSelector('SocialForm');
const AppScreen = connect(
  (state) => ({
    obligations: state.obligation.obligations,
    obligationsLoading: state.obligation.obligationsLoading,
    obligationValue: selector(state, 'obligation'),
    isCreating: state.social.isCreating,
  }),
  (dispatch) => ({
    getObligationListStart: () => dispatch(getObligationListStart())
  }))(reduxForm({
  // a unique name for the form
  form: 'SocialForm',
  touchOnChange: true,
  fields: [
    'obligation',
    'sub_obligation',
    'comment',
    'unit',
    'amount',
    'date'
  ],
  validate
})(AddNewSocialForm));

export default AppScreen;
