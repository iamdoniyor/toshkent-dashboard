import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {
  clearSingleProject,
  getProjectListStart,
  getSingleProjectStart,
  postCreateInvestmentStart,
  postCreateSocialStart,
  toggleModal
} from "../../services/actions";
import scrollTop from "../../utils/scrollTop";
import PageLoader from "../../components/PageLoader/PageLoader";
import Roadmap from "../../components/Roadmap/Roadmap";
import {Button, Col, Container, Row} from "shards-react";
import PageTitle from "../../components/common/PageTitle";
import Table from "../Table";
import moment from "moment";
import history from "../../services/history";
import ModalComp from "../../components/Modal/ModalComp";
import AddNewInvestmentForm from "../AddNewInvestmentForm/AddNewInvestmentForm";
import AddNewSocialForm from "../AddNewSocialForm/AddNewSocialForm";

class ProjectPage extends Component {
  state = {
    activeModalForm: ""
  };

  componentDidMount() {
    const { getSingleProjectStart, match: { params: { proejctId } } } = this.props;
    scrollTop();
    getSingleProjectStart(proejctId)
  }

  componentWillUnmount() {
    const { clearSingleProject, toggleModal } = this.props;
    clearSingleProject();
    toggleModal(false);
  }

  submitInvestment = values => {
    const { project, postCreateInvestmentStart } = this.props;
    postCreateInvestmentStart({...values, project: project._id})
  };

  submitSocial = values => {
    const { project, postCreateSocialStart } = this.props;
    postCreateSocialStart({...values, project: project._id})
  };

  renderForm = () => {
    const { activeModalForm } = this.state;
    const {  isInvestCreating, isSocialCreating } = this.props;

    if (activeModalForm === "investment") {
      return  <AddNewInvestmentForm
          isCreating={isInvestCreating}
          onSubmit={this.submitInvestment}
        />
    } else if (activeModalForm === "social") {
      return  <AddNewSocialForm
      isCreating={isSocialCreating}
      onSubmit={this.submitSocial}
      />
    }
    else return  null
  };

  render() {
    const { project, projectLoading, isModalOpen, toggleModal, isInvestCreating, isSocialCreating } = this.props;
    if (!project || projectLoading) {
      return <PageLoader/>
    }

    const projectBody = [{
      title: "Loyiha raqami",
      value: project.number
    },{
      title: "Protokol",
      value: project.protocolNumber
    },{
      title: "Loyiha nomi",
      value: project.name
    },{
      title: "Tuman",
      value: project.district
    },{
      title: "Kompaniya",
      value: project.companyName
    },{
      title: "Soha",
      value: project.sphere
    },{
      title: "Tarmoq",
      value: project.sector
    },{
      title: "Ijtimoiy majburiyat",
      value: project.social_obligation
    },{
      title: "Ijtimoiy majburiyat (Qo'shimcha)",
      value: `${project.social_sub_obligation ? project.social_sub_obligation : ""}`
    },{
      title: "Ijtimoiy majburiyat muddati",
      value: `${project.social_end && moment(project.social_end).format('DD/MM/YYYY')}`
    },{
      title: "Investitsiya muddati",
      value: `${project.investment_end && moment(project.investment_end).format('DD/MM/YYYY')}`
    },{
      title: "TShH qarori",
      value: project.decision
    }];

    const projectTableBody = projectBody.map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.title}</td>
          <td>{item.value}</td>
        </tr>)
    });

    const socialTableHead = [
      "Majburiyat turi",
      "",
      "Izoh",
      "Birlik",
      "Miqdor",
      "Muddat",
      "Bajarildi"
    ];

    const socialList = (project && project.socialContracts && project.socialContracts.length) ? project.socialContracts.sort((a, b) => new Date(a.date) - new Date(b.date)).map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.obligation}</td>
          <td>{item.sub_obligation ? item.sub_obligation : ""}</td>
          <td>{item.comment}</td>
          <td>{item.unit}</td>
          <td>{item.amount}</td>
          <td>{item.date ? moment(item.date).format('DD/MM/YYYY') : ""}</td>
          <td>{item.completed ? <i className="material-icons" style={{color: "green"}}>check</i> : <i className="material-icons" style={{color: "red"}}>close</i>}</td>
        </tr>
      )
    }) : null;

    const investTableHead = [
      "Miqdor",
      "Muddat",
      "Bajarildi"
    ];

    const investList = (project && project.investmentContracts && project.investmentContracts.length) ? project.investmentContracts.sort((a, b) => new Date(a.date) - new Date(b.date)).map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.amount}</td>
          <td>{item.date ? moment(item.date).format('DD/MM/YYYY') : ""}</td>
          <td>{item.completed ? <i className="material-icons" style={{color: "green"}}>check</i> : <i className="material-icons" style={{color: "red"}}>close</i>}</td>
        </tr>
      )
    }) : null;

     return (
       <Container fluid className="main-content-container px-4">
         <Row>
           <Row noGutters className="page-header py-4">
             <PageTitle title="Loyiha haqida ma'lumot" subtitle="Loyihalar" className="text-sm-left col-sm-12" />
           </Row>
            <Roadmap items={project.roadmap && project.roadmap}/>
         </Row>
          <Row>
            <Col lg={{size:4}}>
              <Table title="Asosiy"
                     header={[]}
                     body={projectTableBody}
              />
              <Table title="Investitsiya majburiyatlari"
                     header={investTableHead}
                     body={investList}
                     button
                     buttonTitle={"Qo'shish"}
                     func={() => {
                       this.setState({activeModalForm: "investment"});
                       toggleModal(true)
                     }}
              />
            </Col>
            <Col lg={{size:8}}>
              <Table title="Ijtimoiy majburiyatlar"
                     header={socialTableHead}
                     body={socialList}
                     button
                     buttonTitle={"Qo'shish"}
                     func={() => {
                       this.setState({activeModalForm: "social"});
                       toggleModal(true)
                     }}
              />
            </Col>
          </Row>
         <ModalComp
           show={isModalOpen}
           onHide={() => toggleModal(false)}
           title={""}
           header={"Investitsiya majburiyati"}
         >
           {this.renderForm()}
         </ModalComp>
       </Container>
     )
  }
}

export default connect(
  (state) => ({
    project: state.project.project,
    projectLoading: state.project.projectLoading,
    isModalOpen: state.modal.isOpen,
    isInvestCreating: state.investment.isCreating,
    isSocialCreating: state.social.isCreating
  }),
  (dispatch) => ({
    getSingleProjectStart: (id) => dispatch(getSingleProjectStart(id)),
    postCreateInvestmentStart: (data) => dispatch(postCreateInvestmentStart(data)),
    postCreateSocialStart: (data) => dispatch(postCreateSocialStart(data)),
    toggleModal: (status) => dispatch(toggleModal(status)),
    clearSingleProject: () => dispatch(clearSingleProject()),
  })
)(ProjectPage);
