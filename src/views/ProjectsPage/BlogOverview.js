import React, {Component} from 'react';
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import PageTitle from "../../components/common/PageTitle";
import SmallStats from "../../components/common/SmallStats";
import UsersOverview from "../../components/blog/UsersOverview";
import UsersByDevice from "../../components/blog/UsersByDevice";
import NewDraft from "../../components/blog/NewDraft";
import Discussions from "../../components/blog/Discussions";
import TopReferrals from "../../components/common/TopReferrals";
import {connect} from "react-redux";
import {
  getProjectListStart,
  getSingleProjectStart
} from "../../services/actions";
import Table from "../Table";
import style from "./styles.css";
import moment from "moment";
import 'moment/locale/uz-latn'
import history from "../../services/history";
import scrollTop from "../../utils/scrollTop";
import ProjectFilterForm from "../ProjectFilterForm/ProjectFilterForm";

class BlogOverview extends Component {
  componentDidMount() {
    scrollTop();
    this.props.getProjectListStart()
  }

  submitFilters = values => {
    const { getProjectListStart, projectFilters } = this.props;
    let filters = values;

    if (filters.sector || filters.social_sub_obligation) {
      if (filters.sector) {
        if (projectFilters.sphere.find(i => i.name === filters.sphere) && projectFilters.sphere.find(i => i.name === filters.sphere).sectors && projectFilters.sphere.find(i => i.name === filters.sphere).sectors.find(it => it === filters.sector) )  {
          getProjectListStart(filters)
        } else {
          delete filters.sector;
          getProjectListStart(filters)
        }
      } else if (filters.social_sub_obligation) {
        if (projectFilters.obligation.find(i => i.name === filters.social_obligation) && projectFilters.obligation.find(i => i.name === filters.social_obligation).subcategory && projectFilters.obligation.find(i => i.name === filters.social_obligation).subcategory.find(it => it === filters.social_sub_obligation) )  {
          getProjectListStart(filters)
        } else {
          delete filters.social_sub_obligation;
          getProjectListStart(filters)
        }
      }
    } else getProjectListStart(filters)
  };

  render() {
    const {smallStats, projects, getSingleProjectStart} = this.props;
    const tableHeaders = [
      "#",
      "Protokol",
      "Loyiha",
      "Tuman",
      "MChJ",
      "Soha",
      "Tarmoq",
      "Ijtimoiy majburiyat",
      "Muddat (ijtimoiy)",
      "Investitsiya muddati",
      "TShH qarori",
      "Tugatildi",
      "Tugallangan sana"
    ];

    const projectList = projects && projects.sort((a, b) => b.number - a.number).map((item, i) => {
      return (
        <tr key={i} className="table-row" onClick={() => history.push(`projects/${item._id}`)}>
          <td>{item.number}</td>
          <td>{item.protocolNumber}</td>
          <td>{item.name}</td>
          <td>{item.district}</td>
          <td>{item.companyName}</td>
          <td>{item.sphere}</td>
          <td>{item.sector}</td>
          <td>{item.social_obligation} {item.social_sub_obligation && item.social_sub_obligation}</td>
          <td>{item.social_end && moment(item.social_end).locale("uz").format('DD/MM/YYYY')}</td>
          <td>{item.investment_end && moment(item.investment_end).format('DD/MM/YYYY')}</td>
          <td>{item.decision}</td>
          <td>{item.completed ? <i className="material-icons" style={{color: "green"}}>check</i> : <i className="material-icons" style={{color: "red"}}>close</i>}</td>
          <td>{item.completedAt ? moment(item.completedAt).format('DD/MM/YYYY') : "--"}</td>
        </tr>
      )
    });

    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle title="Loyihalar" subtitle="Asosiy sahifa" className="text-sm-left mb-3" />
        </Row>
        <Row>
          <Col>
            <ProjectFilterForm onSubmit={this.submitFilters}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table title={`Loyihalar soni: ${projects && projects.length}`}
                   header={tableHeaders}
                   body={projectList}
            />
          </Col>
        </Row>
        {/* Small Stats Blocks */}
        {/*<Row>*/}
        {/*  {smallStats.map((stats, idx) => (*/}
        {/*    <Col className="col-lg mb-4" key={idx} {...stats.attrs}>*/}
        {/*      <SmallStats*/}
        {/*        id={`small-stats-${idx}`}*/}
        {/*        variation="1"*/}
        {/*        chartData={stats.datasets}*/}
        {/*        chartLabels={stats.chartLabels}*/}
        {/*        label={stats.label}*/}
        {/*        value={stats.value}*/}
        {/*        percentage={stats.percentage}*/}
        {/*        increase={stats.increase}*/}
        {/*        decrease={stats.decrease}*/}
        {/*      />*/}
        {/*    </Col>*/}
        {/*  ))}*/}
        {/*</Row>*/}

        {/*<Row>*/}
        {/*  /!* Users Overview *!/*/}
        {/*  <Col lg="8" md="12" sm="12" className="mb-4">*/}
        {/*    <UsersOverview />*/}
        {/*  </Col>*/}

        {/*  /!* Users by Device *!/*/}
        {/*  <Col lg="4" md="6" sm="12" className="mb-4">*/}
        {/*    <UsersByDevice />*/}
        {/*  </Col>*/}

        {/*  /!* New Draft *!/*/}
        {/*  <Col lg="4" md="6" sm="12" className="mb-4">*/}
        {/*    <NewDraft />*/}
        {/*  </Col>*/}

        {/*  /!* Discussions *!/*/}
        {/*  <Col lg="5" md="12" sm="12" className="mb-4">*/}
        {/*    <Discussions />*/}
        {/*  </Col>*/}

        {/*  /!* Top Referrals *!/*/}
        {/*  <Col lg="3" md="12" sm="12" className="mb-4">*/}
        {/*    <TopReferrals />*/}
        {/*  </Col>*/}
        {/*</Row>*/}
      </Container>
    );
  }
}


BlogOverview.propTypes = {
  /**
   * The small stats dataset.
   */
  smallStats: PropTypes.array
};

BlogOverview.defaultProps = {
  smallStats: [
    {
      label: "Posts",
      value: "2,390",
      percentage: "4.7%",
      increase: true,
      chartLabels: [null, null, null, null, null, null, null],
      attrs: { md: "6", sm: "6" },
      datasets: [
        {
          label: "Today",
          fill: "start",
          borderWidth: 1.5,
          backgroundColor: "rgba(0, 184, 216, 0.1)",
          borderColor: "rgb(0, 184, 216)",
          data: [1, 2, 1, 3, 5, 4, 7]
        }
      ]
    },
    {
      label: "Pages",
      value: "182",
      percentage: "12.4",
      increase: true,
      chartLabels: [null, null, null, null, null, null, null],
      attrs: { md: "6", sm: "6" },
      datasets: [
        {
          label: "Today",
          fill: "start",
          borderWidth: 1.5,
          backgroundColor: "rgba(23,198,113,0.1)",
          borderColor: "rgb(23,198,113)",
          data: [1, 2, 3, 3, 3, 4, 4]
        }
      ]
    },
    {
      label: "Comments",
      value: "8,147",
      percentage: "3.8%",
      increase: false,
      decrease: true,
      chartLabels: [null, null, null, null, null, null, null],
      attrs: { md: "4", sm: "6" },
      datasets: [
        {
          label: "Today",
          fill: "start",
          borderWidth: 1.5,
          backgroundColor: "rgba(255,180,0,0.1)",
          borderColor: "rgb(255,180,0)",
          data: [2, 3, 3, 3, 4, 3, 3]
        }
      ]
    },
    {
      label: "New Customers",
      value: "29",
      percentage: "2.71%",
      increase: false,
      decrease: true,
      chartLabels: [null, null, null, null, null, null, null],
      attrs: { md: "4", sm: "6" },
      datasets: [
        {
          label: "Today",
          fill: "start",
          borderWidth: 1.5,
          backgroundColor: "rgba(255,65,105,0.1)",
          borderColor: "rgb(255,65,105)",
          data: [1, 7, 1, 3, 1, 4, 8]
        }
      ]
    },
    {
      label: "Subscribers",
      value: "17,281",
      percentage: "2.4%",
      increase: false,
      decrease: true,
      chartLabels: [null, null, null, null, null, null, null],
      attrs: { md: "4", sm: "6" },
      datasets: [
        {
          label: "Today",
          fill: "start",
          borderWidth: 1.5,
          backgroundColor: "rgb(0,123,255,0.1)",
          borderColor: "rgb(0,123,255)",
          data: [3, 2, 3, 2, 4, 5, 4]
        }
      ]
    }
  ]
};




export default connect(
  (state) => ({
    projects: state.project.projects,
    projectFilters: state.projectFilters.filters
  }),
  (dispatch) => ({
    getProjectListStart: (filters) => dispatch(getProjectListStart(filters)),
    getSingleProjectStart: (id) => dispatch(getSingleProjectStart(id)),
  })
)(BlogOverview);
