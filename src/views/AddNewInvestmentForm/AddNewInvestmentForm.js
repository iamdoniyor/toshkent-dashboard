import React from 'react';
import {Field, formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {
  getObligationListStart
} from "../../services/actions";
import CustomFormInput from "../../components/FormInput/FormInput";
import Wrapper from "../../containers/Wrapper";
import DatePickerComp from "../../components/DatePicker/DatePicker";
import {Button, ListGroupItem} from "shards-react";

const AddNewInvestmentForm = ({isCreating, handleSubmit}) => {
  return (
    <Wrapper>
      <Field
        name="amount"
        component={CustomFormInput}
        title={"Investitsiya qiymati"}
        number
      />
      <Field
        name="date"
        component={DatePickerComp}
        title={"Investitsiya majburiyati muddati"}
      />
      <Button outline={isCreating} disabled={isCreating} onClick={(props) => !isCreating ? handleSubmit(props) : null}>{isCreating ? "Saqlanmoqda... " : "Saqlash"}</Button>
    </Wrapper>
  );
};

const validate = formValues => {
  const errors = {};

  if (!formValues.amount) {
    errors.amount = "Investitsiya qiymatini kiriting"
  }

  if (!formValues.date) {
    errors.date = "Investitsiya majburiyati muddatini tanlang"
  }

  return errors;
};

const selector = formValueSelector('InvestmentForm');
const AppScreen = connect(
  (state) => ({
    obligations: state.obligation.obligations,
    obligationsLoading: state.obligation.obligationsLoading,
    obligationValue: selector(state, 'social_obligation')
  }),
  (dispatch) => ({
    getObligationListStart: () => dispatch(getObligationListStart())
  }))(reduxForm({
  // a unique name for the form
  form: 'InvestmentForm',
  touchOnChange: true,
  fields: [
    'amount',
    'date'
  ],
  validate
})(AddNewInvestmentForm));

export default AppScreen;
