import React, {Component} from 'react';
import {
  Button,
  Card,
  Col,
  Container,
  FormInput,
  Row
} from "shards-react";
import PageTitle from "../../components/common/PageTitle";
import {connect} from "react-redux";
import {
  getDistrictListStart,
  getObligationListStart,
  getSphereListStart
} from "../../services/actions";
import Table from "../Table";
import Wrapper from "../../containers/Wrapper";
import URL from "../../services/api/config";
import axios from "axios";
import scrollTop from "../../utils/scrollTop";

class Categories extends Component {
  state = {
    showSphereInput: false,
    sphereLoading: false,
    showSectorInput: false,
    sectorLoading: false,
    sphereName: "",
    sphereId: null,
    sectorName: "",
    showObligationInput: false,
    obligationLoading: false,
    subObligationLoading: false,
    showSubObligationInput: false,
    obligationName: "",
    obligationId: null,
    subObligationName: "",
    showDistrictInput: false,
    districtLoading: false,
    districtName: ""
  };

  componentDidMount() {
    const {getDistrictListStart,
      getObligationListStart,
      getSphereListStart} = this.props;
    scrollTop()
    getDistrictListStart();
    getObligationListStart();
    getSphereListStart()
  }

  postName = (name, loading, field, func, url) => {
    const value = this.state[name];
    const fun = this.props[func];

    this.setState({[loading]: true});

    axios.post(`${URL}/${url}`, {
      name: value
    }).then(() => {
      this.setState({
        [loading]: false,
        [name]: "",
        [field]: false});
      fun()
    }).catch(() => {
      this.setState({
        [loading]: false,
        [name]: "",
        [field]: false});
    })
  };

  postSubName = (name, loading, field, func, url) => {
    const value = this.state[name];
    const fun = this.props[func];

    this.setState({[loading]: true});

    axios.post(`${URL}/${url}`, {
      name: value
    }).then(() => {
      this.setState({
        [loading]: false,
        [name]: "",
        [field]: false});
      fun()
    }).catch(() => {
      this.setState({
        [loading]: false,
        [name]: "",
        [field]: false});
    })
  };

  render() {
    const { spheres, obligations, districts } = this.props;
    const { showSphereInput,
      sphereLoading,
      showObligationInput,
      obligationLoading,
      districtLoading,
      showDistrictInput,
      showSectorInput,
      showSubObligationInput,
      subObligationLoading,
      sectorLoading,
      sphereId,
      obligationId} = this.state;

    const sphereTableHeaders = [
      "Soha nomi",
      "Tarmoq",
      ""
    ];

    const sphereList = spheres && spheres.map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.name}</td>
          <td>{item.sectors && item.sectors.map((sector, id)=> (<Wrapper key={id}>{sector.name} <br/></Wrapper>))}</td>
          <td>
            <Button className="my-2" onClick={() => this.setState({showSectorInput: !showSectorInput, sphereId: item._id})}>{(showSectorInput && sphereId === item._id) ? "Yopish" : "Tarmoq Qo'shish"}</Button>
            {(showSectorInput && sphereId === item._id) ?
              <Wrapper>
                <div className="mb-3">
                  <label htmlFor="feEmailAddress">Tarmoqning nomi</label>
                  <FormInput
                    type="text"
                    placeholder=""
                    onChange={e => {
                      e.preventDefault()
                      this.setState({
                        sectorName: e.target.value
                      })
                    }}
                  />
                </div>
                <div>
                  <Button outline={sectorLoading}
                          disabled={sectorLoading}
                          onClick={() => !sectorLoading ? this.postSubName("sectorName", "sectorLoading", "showSectorInput", "getSphereListStart", `spheres/${item._id}/sectors` ) : null}
                          theme="primary"
                          className="mb-2 mr-1">
                    {(sectorLoading && sphereId === item._id) ?
                      "Saqlanmoqda..." : "Saqlash"}
                  </Button>
                </div>
              </Wrapper>
              : null}
          </td>
        </tr>
      )
    });

    const obligationTableHeaders = [
      "Ijtimoiy majburiyat nomi",
      "Qo'shimcha",
      ""
    ];

    const obligationList = obligations && obligations.map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.name}</td>
          <td>{item.subcategories && item.subcategories.map((sector, id)=> (<Wrapper key={id}>{sector.name} <br/></Wrapper>))}</td>
          <td>
            <Button className="my-2" onClick={() => this.setState({showSubObligationInput: !showSubObligationInput, obligationId: item._id})}>{(showSubObligationInput && obligationId === item._id) ? "Yopish" : "Qo'shimcha Qo'shish"}</Button>
            {(showSubObligationInput && obligationId === item._id) ?
              <Wrapper>
                <div className="mb-3">
                  <label htmlFor="feEmailAddress">Ijtimoiy majburiyat nomi</label>
                  <FormInput
                    type="text"
                    placeholder=""
                    onChange={e => {
                      e.preventDefault()
                      this.setState({
                        subObligationName: e.target.value
                      })
                    }}
                  />
                </div>
                <div>
                  <Button outline={subObligationLoading}
                          disabled={subObligationLoading}
                          onClick={() => !subObligationLoading ? this.postSubName("subObligationName", "subObligationLoading", "showSubObligationInput", "getObligationListStart", `obligations/${item._id}/subcategory` ) : null}
                          theme="primary"
                          className="mb-2 mr-1">
                    {(subObligationLoading && obligationId === item._id) ?
                      "Saqlanmoqda..." : "Saqlash"}
                  </Button>
                </div>
              </Wrapper>
              : null}
          </td>
        </tr>
      )
    });

    const districtTableHeaders = [
      "Tuman"
    ];

    const districtList = districts && districts.map((item, i) => {
      return (
        <tr key={i}>
          <td>{item.name}</td>
       </tr>
      )
    });



    return (
      <Container fluid className="main-content-container px-4">
        <Row>
          <Col lg={{size:6}}>
            <Row noGutters className="page-header py-4">
              <PageTitle title="Soha va tarmoqlar" subtitle="Kategoriyalar" className="text-sm-left col-sm-12" />
              <Button className="my-2" onClick={() => this.setState({showSphereInput: !showSphereInput})}>{showSphereInput ? "Yopish" : "Qo'shish"}</Button>
            </Row>
            {showSphereInput ?
            <Wrapper>
              <Row noGutters className="page-header py-2">
                <Col md="6">
                  <label htmlFor="feEmailAddress">Sohaning nomi</label>
                  <FormInput
                    id="name"
                    type="text"
                    placeholder=""
                    onChange={e => {
                      e.preventDefault()
                      this.setState({
                        sphereName: e.target.value
                      })
                    }}
                  />
                </Col>
                <Col md="6">
                </Col>
              </Row>
              <Row noGutters className="page-header py-4">
                <Button outline={sphereLoading}
                        disabled={sphereLoading}
                        onClick={() => !sphereLoading ? this.postName("sphereName", "sphereLoading", "showSphereInput", "getSphereListStart", "spheres" ) : null}
                        theme="primary"
                        className="mb-2 mr-1">
                  {!sphereLoading ?
                  "Saqlash" : "Saqlanmoqda..."}
                </Button>
              </Row>
            </Wrapper> : null}
            <Row>
              <Col>
                  <Table title=""
                         header={sphereTableHeaders}
                         body={sphereList}
                  />

              </Col>
            </Row>
          </Col>
          <Col lg={{size:6}}>
            <Row noGutters className="page-header py-4">
              <PageTitle title="Ijtimoiy majburiyatlar" subtitle="Kategoriyalar" className="text-sm-left col-sm-12" />
              <Button className="my-2" onClick={() => this.setState({showObligationInput: !showObligationInput})}>{showObligationInput ? "Yopish" : "Qo'shish"}</Button>
            </Row>
            {showObligationInput ?
              <Wrapper>
                <Row noGutters className="page-header py-2">
                <Col md="6">
                  <label htmlFor="feEmailAddress">Ijtimoiy majburiyat nomi</label>
                  <FormInput
                    id="name"
                    type="text"
                    placeholder=""
                    onChange={e => {
                      e.preventDefault()
                      this.setState({
                        obligationName: e.target.value
                      })
                    }}
                  />
                </Col>
                <Col md="6">
                </Col>
              </Row>
                <Row noGutters className="page-header py-4">
                  <Button outline={obligationLoading}
                          disabled={obligationLoading}
                          onClick={() => !obligationLoading ? this.postName("obligationName", "obligationLoading", "showObligationInput", "getObligationListStart", "obligations" ) : null}
                          theme="primary"
                          className="mb-2 mr-1">
                    {!obligationLoading ?
                      "Saqlash" : "Saqlanmoqda..."}
                  </Button>
                </Row>
              </Wrapper>
              : null}
            <Row>
              <Col>
                  <Table title=""
                         header={obligationTableHeaders}
                         body={obligationList}
                  />

              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
            <Col lg={{size:6}}>
              <Row noGutters className="page-header py-4">
                <PageTitle title="Tumanlar" subtitle="Kategoriyalar" className="text-sm-left col-sm-12" />
                <Button className="my-2" onClick={() => this.setState({showDistrictInput: !showDistrictInput})}>{showDistrictInput ? "Yopish" : "Qo'shish"}</Button>
              </Row>
              {showDistrictInput ?
                <Wrapper>
                  <Row noGutters className="page-header py-2">
                    <Col md="6">
                      <label htmlFor="feEmailAddress">Tuman nomi</label>
                      <FormInput
                        id="name"
                        type="text"
                        placeholder=""
                        onChange={e => {
                          e.preventDefault()
                          this.setState({
                            districtName: e.target.value
                          })
                        }}
                      />
                    </Col>
                    <Col md="6">
                    </Col>
                  </Row>
                  <Row noGutters className="page-header py-4">
                    <Button outline={districtLoading}
                            disabled={districtLoading}
                            onClick={() => !districtLoading ? this.postName("districtName", "districtLoading", "showDistrictInput", "getDistrictListStart", "districts" ) : null}
                            theme="primary"
                            className="mb-2 mr-1">
                      {!districtLoading ?
                        "Saqlash" : "Saqlanmoqda..."}
                    </Button>
                  </Row>
                </Wrapper>
                : null}
              <Row>
                <Col>
                    <Table title=""
                           header={districtTableHeaders}
                           body={districtList}
                    />
                </Col>
              </Row>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  (state) => ({
    spheres: state.sphere.spheres,
    obligations: state.obligation.obligations,
    districts: state.district.districts
  }),
  (dispatch) => ({
    getSphereListStart: () => dispatch(getSphereListStart()),
    getObligationListStart: () => dispatch(getObligationListStart()),
    getDistrictListStart: () => dispatch(getDistrictListStart()),
  })
)(Categories);
