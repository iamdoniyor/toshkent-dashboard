import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Card, Col, Container, ListGroupItem, Row, FormSelect} from "shards-react";
import PageTitle from "../../components/common/PageTitle";
import {connect} from "react-redux";
import {
  changeNotificationPeriod,
  getNotificationListStart
} from "../../services/actions";
import NotificationCard
  from "../../components/NotificationCard/NotificationCard";
import history from "../../services/history";
import PageLoader from "../../components/PageLoader/PageLoader";

class NotificationsPage extends Component {
  render() {
    const { notifications, notificationPeriod, changeNotificationPeriod, getNotificationListStart, notificationsLoading } = this.props;

    return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle
            sm="4"
            title="Eslatmalar"
            className="text-sm-left"
          />
          <div>
            <FormSelect value={notificationPeriod}  onChange={(e) => {
              changeNotificationPeriod(e.target.value);
              getNotificationListStart(e.target.value);
            }}>
              <option value="30">30 kun</option>
              <option value="60">60 kun</option>
            </FormSelect>
          </div>
        </Row>

        <Row>
          {notificationsLoading ? <PageLoader/> :
          <Col lg={{ size: 8, order: 2, offset: 2 }}
               className="mb-4">
            {notifications && notifications.map((item,id) => {
                return <Card key={id} small className="mb-2 pointer" onClick={() => history.push(`/projects/${item.project && item.project._id}`)}>
                    <NotificationCard key={id} notification={item}/>
                </Card>
              }
            )}
          </Col>}
        </Row>
      </Container>
    );
  }
}

export default connect(
  (state) => ({
    notifications: state.notification.notifications,
    notificationsLoading: state.notification.notificationsLoading,
    notificationPeriod: state.notification.period
  }),
  (dispatch) => ({
    getNotificationListStart: (days) => dispatch(getNotificationListStart(days)),
    changeNotificationPeriod: (days) => dispatch(changeNotificationPeriod(days)),
  })
)(NotificationsPage);
