import React, {Component} from 'react';
import AddNewProjectForm from "../AddNewProjectForm/AddNewProjectForm";
import {connect} from "react-redux";
import {getProjectListStart, postCreateProjectStart} from "../../services/actions";
import scrollTop from "../../utils/scrollTop";

class AddNewProject extends Component {
  componentDidMount() {
    scrollTop()
  }

  submit = values => {

    this.props.postCreateProjectStart(values)
  };

  render() {
    return (
      <AddNewProjectForm onSubmit={this.submit} />
    );
  }
}

export default connect(
  (state) => ({
    projects: state.project.projects
  }),
  (dispatch) => ({
    postCreateProjectStart: (data) => dispatch(postCreateProjectStart(data)),
  })
)(AddNewProject);
