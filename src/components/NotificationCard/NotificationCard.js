import React from 'react';
import { DropdownItem} from "shards-react";
import moment from "moment";


const NotificationCard = ({notification}) => {
  return (
    <DropdownItem>
      <div className="notification__content">
        <div className="notification__category__container">
          <div className="notification__icon-wrapper">
            <div className="notification__icon">
              <i className="material-icons">{notification.obligation ? "emoji_transportation" : "attach_money"}</i>
            </div>
          </div>
          <span className="notification__category">{notification.obligation ? "IJTIMOIY" : "INVESTITSIYA"}</span>
          <span className="notification__category">{notification.date && moment(notification.date).format('DD MMMM YYYY')}</span>
        </div>
          <h5>{notification.project && notification.project.number}</h5>
          <h5>{notification.project && notification.project.name}</h5>
        <h6>{notification.obligation && notification.obligation} {notification.amount && notification.amount} {notification.unit && notification.unit}
        </h6>
      </div>
    </DropdownItem>
  );
};

export default NotificationCard;
