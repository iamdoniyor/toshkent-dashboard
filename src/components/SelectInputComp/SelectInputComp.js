import React, {Component} from 'react';
import {FormFeedback, FormGroup, FormSelect} from "shards-react";
import Wrapper from "../../containers/Wrapper";

class SelectInputComp extends Component {
  // handleNumber = (textValue) => {
  //   let text = textValue.replace(/\s/g, '').replace(/[^0-9]/g, "");
  //   if (this.props.percentDiscount && text.length > 2 ) {
  //     return null
  //   } else {
  //     if (this.props.amountVal && parseInt(this.props.amountVal) < parseInt(text)) {
  //       return null
  //     } else {
  //       text = text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  //       this.props.input.onChange(text)
  //     }
  //   }
  // };

  render() {
      const { loading, input, input: { value, onChange }, options, meta: { error, touched }, title, placeholder, validationDisabled, inputDisabled} = this.props;
      return (
        <FormGroup>
          <label htmlFor="">{title}</label>
          <FormSelect
            value={value}
            disabled={inputDisabled}
            invalid={validationDisabled ? null : error && touched}
            valid={validationDisabled ? null : !error && touched}
            {...input}
          >
            <option value={''}>{loading ? "Yuklanmoqda..." : "Tanlash"}</option>
            {(options && !loading) ? options.map((item, i) => {
              return <option key={i} value={item.name}>{item.name}</option>
            }) : null}
          </FormSelect>
          {validationDisabled ? null :
          <Wrapper>
            {(error && touched) ?
              <FormFeedback>{error}</FormFeedback>
              : <div className="invalid-feedback" style={{color: "transparent", display: "block"}}>.</div>
            }
          </Wrapper>}
        </FormGroup>
      )
  }
}

export default SelectInputComp;
