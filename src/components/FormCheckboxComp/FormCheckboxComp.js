import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FormCheckbox} from "shards-react";

class FormCheckboxComp extends Component {
  render() {
    const { loading, input, input: { value, onChange }, options, meta: { error, touched }, title, placeholder, validationDisabled, inputDisabled} = this.props;

    return (
      <FormCheckbox
        checked={value}
        {...input}
        onChange={() => onChange(!value)}
      >
        {title}
      </FormCheckbox>
    );
  }
}

FormCheckboxComp.propTypes = {};

export default FormCheckboxComp;
