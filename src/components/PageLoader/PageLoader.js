import React from 'react';
import {ScaleLoader} from "react-spinners";

const divStyle = {
  display: "flex",
  height: "100vh",
  flex: 1,
  alignItems: "center",
  justifyContent: "center"
};


const PageLoader = () => {
  return (
    <div style={divStyle}>
      <ScaleLoader
        size={200}
        color={"#007bff"}
        loading={true}
      />
    </div>
  );
};

export default PageLoader;
