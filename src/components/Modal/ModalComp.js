import React from 'react';
import {Modal} from "react-bootstrap";
import {Button} from "shards-react";

const ModalComp = (props) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.header}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>{props.title}</h4>
          {props.children}
      </Modal.Body>
      {/*<Modal.Footer>*/}
      {/*  <Button onClick={props.onHide}>{props.buttonText}</Button>*/}
      {/*</Modal.Footer>*/}
    </Modal>
  );
};

export default ModalComp;
