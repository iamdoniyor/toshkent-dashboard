import React, {Component} from 'react';
import DatePicker from "react-datepicker";
import { registerLocale, setDefaultLocale } from  "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import "./styles.css";
import uz from 'date-fns/locale/uz';
import {FormFeedback, FormGroup} from "shards-react";
import {checkDate} from "../../utils/checkDate";
import moment from "moment";
import Wrapper from "../../containers/Wrapper";
registerLocale('uz', uz);



class DatePickerComp extends Component {
  state = {
    startDate: moment().valueOf()
  };

  render() {
    const { input, input: { value, onChange, onBlur, onFocus }, meta: { error, touched }, title, placeholder, validationDisabled} = this.props;
    const { startDate } = this.state;
    let styleName;
    if (error && touched) {
      styleName = "form-control is-invalid"
    } else if (!error && touched) {
      styleName = "form-control is-valid"
    } else {
      styleName = "form-control"
    }

    if (validationDisabled) {
      styleName = "form-control"
    }


    return (
      <FormGroup>
        <label >{title}</label><br/>
        <DatePicker
          dateFormat="dd MMMM yyyy"
          locale="uz"
          placeholderText="Sanani tanlang"
          selected={value}
          className={styleName}
          onChange={(date) => {
            return checkDate(date) ? onChange(date) : null}}
          {...input}
          onBlur={e => checkDate(e.target.value) ? onBlur(e.target) : null}
          onFocus={onFocus}
        />
        {validationDisabled ? null :
          <Wrapper>
            {(error && touched) ?
              <FormFeedback>{error}</FormFeedback>
              : null
            }
          </Wrapper>
        }
      </FormGroup>
    );
  }
}

export default DatePickerComp;
