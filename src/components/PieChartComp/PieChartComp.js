import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody
} from "shards-react";

import Chart from "../../utils/chart";

class PieChartComp extends React.Component {
  constructor(props) {
    super(props);

    this.canvasRef = React.createRef();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    function hexToRgb(hex) {
      let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    }

    if ( this.props.chartDataArray === nextProps.chartDataArray ) {
      return
    } else {
      const { chartDataArray } = nextProps;
      let colors = [
        "#ee7f00",
        "#2ec4b6",
        "#102E4A",
        "#e71d36",
        "#f0c987",
        "#157f1f",
        "#4cb963",
        "#fabc3c",
        "#034748",
        "#238fed",
        "#ff595e",
        "#ffca3a",
        "#97cc04",
        "#01295f"
      ];

      colors = colors.map(item => {
        return `rgb(${hexToRgb(item).r}, ${hexToRgb(item).g}, ${hexToRgb(item).b})`
      });

      const chartConfig = {
        type: "pie",
        data: {
          datasets: [
            {
              hoverBorderColor: "#ffffff",
              data: chartDataArray.map(item => item.num),
              backgroundColor: colors
            }
          ],
          labels: chartDataArray.map(item => item && item.name)
        },
        options: {
          ...{
            legend: {
              position: "bottom",
              labels: {
                padding: 25,
                boxWidth: 20
              }
            },
            cutoutPercentage: 0,
            tooltips: {
              custom: false,
              mode: "index",
              position: "nearest"
            },
            responsive: true
          },
          tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
              label: function(tooltipItems, data) {
                return `${data.labels[tooltipItems.index]}: ${data.datasets[0].data[tooltipItems.index]}`;
              }
            }
          },
          ...this.props.chartOptions
        }
      };

      new Chart(this.canvasRef.current, chartConfig);
    }
  }

  render() {
    const { title } = this.props;

    return (
      <Card small className="h-100">
        <CardHeader className="border-bottom">
          <h6 className="m-0">{title}</h6>
        </CardHeader>
        <CardBody className="d-flex py-0">
          <canvas
            height="200"
            ref={this.canvasRef}
            className="blog-users-by-device m-auto"
          />
        </CardBody>
      </Card>
    );
  }
}

PieChartComp.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The chart config object.
   */
  chartConfig: PropTypes.object,
  /**
   * The Chart.js options.
   */
  chartOptions: PropTypes.object,
  /**
   * The chart data.
   */
  chartData: PropTypes.object
};

export default PieChartComp;
