import React, {Component} from 'react';
import {FormFeedback, FormGroup, FormInput} from "shards-react";

class CustomFormInput extends Component {
  handleNumber = (text) => {
    let newText = text.replace(/\s/g, '').replace(/[^0-9]/g, "");
    newText = newText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    this.props.input.onChange(newText)
  };

  textChange = (text) => {
    this.props.input.onChange(text)
  };

  render() {
    const { input, input: { value }, meta: { error, touched }, title, placeholder, number} = this.props;
    return (
        <FormGroup>
          <label >{title}</label>
          <FormInput
            value={value}
            placeholder={placeholder}
            required
            invalid={error && touched}
            valid={!error && touched}
            {...input}
            onChange={(e) => number ? this.handleNumber(e.target.value) : this.textChange(e.target.value)}
          />
          {(error && touched) ?
          <FormFeedback>{error}</FormFeedback>
            : <div className="invalid-feedback" style={{color: "transparent", display: "block"}}>.</div>
          }
        </FormGroup>
    );
  }
}

export default CustomFormInput;
