import React from "react";
import { NavItem, NavLink, Badge, Collapse, DropdownItem } from "shards-react";
import {connect} from "react-redux";
import moment from "moment";

import history from "../../../../services/history";

import {getNotificationListStart} from "../../../../services/actions";

 class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleNotifications = this.toggleNotifications.bind(this);
  }

   toggleNotifications() {
    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    const { notifications } = this.props;
    const notificationList = notifications && notifications.slice(0, 4);
    console.log("notificationList ", notificationList)
    return (
      <NavItem className="border-right dropdown notifications pointer">
        <NavLink
          className="nav-link-icon text-center"
          onClick={this.toggleNotifications}
        >
          <div className="nav-link-icon__wrapper">
            <i className="material-icons">&#xE7F4;</i>
            <Badge pill theme="danger">
              {notifications && notifications.length}
            </Badge>
          </div>
        </NavLink>
        <Collapse
          open={this.state.visible}
          className="dropdown-menu dropdown-menu-small"
        >
          {notificationList && notificationList.map((item, id) => {
            return <DropdownItem key={id} onClick={() => history.push(`/projects/${item.project && item.project._id}`)}>
              <div className="notification__icon-wrapper">
                <div className="notification__icon">
                  <i className="material-icons">{item.obligation ? "emoji_transportation" : "attach_money"}</i>
                </div>
              </div>
              <div className="notification__content">
                <div className="notification__category__container">
                  <span className="notification__category">{item.obligation ? "Ijtimoiy" : "Investitsiya"}</span>
                  <span className="notification__category">{item.date && moment(item.date).format('DD MMMM YYYY')}</span>
                </div>
                <p>
                  {item.project && item.project.number}. {item.project && item.project.name} : {" "}
                  {item.obligation && item.obligation} {item.amount && item.amount} {item.unit && item.unit}
                </p>
              </div>
            </DropdownItem>
          })}
          <DropdownItem className="notification__all text-center" onClick={() => history.push("/notifications")}>
            Barcha Eslatmalar
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}

export default connect(
  (state) => ({
    notifications: state.notification.notifications,
    notificationsLoading: state.notification.notificationsLoading
  }),
  (dispatch) => ({
  })
)(Notifications);
