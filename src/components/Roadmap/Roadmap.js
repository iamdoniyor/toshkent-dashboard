import React from 'react';
import moment from "moment";
import "./styles.css"
import Wrapper from "../../containers/Wrapper";

const Roadmap = ({items}) => {
  const points = items && items.sort((a, b) => new Date(b.date) - new Date(a.date));
  return (
    <div className="items mb-4">
        {points && points.map((item, i) => {
          return <Wrapper key={i}>
            <div className={`item btn  btn-sm ${item.completed ? "btn-success" : "btn-white"}`}>
              {item.date ? moment(item.date).format('DD/MM/YYYY') : null}
              <br/>
              {item.name && item.name}
              <br/>
              {item.company && item.company}: {item.person && item.person}
            </div>
            {(i === (points.length - 1)) ? null : <div className="line">
              <i className="material-icons">arrow_right_alt</i>
            </div>}
          </Wrapper>
        })}
    </div>
      )
};

export default Roadmap;
