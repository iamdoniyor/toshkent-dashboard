import React from "react"
import {Redirect, Route, Switch, withRouter} from "react-router-dom"
import {DefaultLayout} from "./layouts";
import Finance from "./views/Finance/Finance";
import Materials from "./views/Materials/Materials";
// import BlogOverview from "./views/BlogOverview";
import Products from "./views/Products/Products";
import ProductPage from "./views/Products/ProductPage/ProductPage";
import StoreStat from "./views/StoreStat/StoreStat";
// import UserProfileLite from "./views/UserProfileLite";
// import AddNewPost from "./views/AddNewPost";
import Errors from "./views/Errors";
// import ComponentsOverview from "./views/ComponentsOverview";
// import Tables from "./views/Tables";
// import BlogPosts from "./views/BlogPosts";
import {connect} from "react-redux";
import { getProductGraph} from "./services/actions";
import LoginPage from "./views/LoginPage/LoginPage";
import Shipments from "./views/Shipments/Shipments";
import Contracts from "./views/Contracts/Contracts";
import ContractPage from "./views/Contracts/ContractPage/ContractPage";
import DocumentPage from "./views/DocumentPage/DocumentPage";
import MoneyPage from "./views/MoneyPage/MoneyPage";
import StaffPage from "./views/StaffPage/StaffPage";
import ShipmentPage from "./views/Shipments/ShipmentPage/ShipmentPage";
import Stock from "./views/Stock/Stock";
import Welcome from "./views/Welcome/Welcome";

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
);

const AltLayout = props => props.children;

const privateRoutes =  [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/finance" />
  },
  {
    path: "/login",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/finance" />
  },
  // {
  //   path: "/blog-overview",
  //   layout: DefaultLayout,
  //   component: BlogOverview
  // },
  {
    path: "/finance",
    layout: DefaultLayout,
    component: Finance
  },
  {
    path: "/materials",
    layout: DefaultLayout,
    component: Materials
  },{
    path: "/products",
    layout: DefaultLayout,
    component: Products
  },{
    path: "/products/:id",
    layout: DefaultLayout,
    component: ProductPage
  },{
    path: "/store",
    layout: DefaultLayout,
    component: StoreStat
  },{
    path: "/shipments",
    layout: DefaultLayout,
    component: Shipments
  },{
    path: "/shipments/:id",
    layout: DefaultLayout,
    component: ShipmentPage
  },{
    path: "/contracts",
    layout: DefaultLayout,
    component: Contracts
  },{
    path: "/contracts/:id",
    layout: DefaultLayout,
    component: ContractPage
  },{
    path: "/contracts/:cid/document/:did",
    layout: DefaultLayout,
    component: DocumentPage
  },{
    path: "/staff",
    layout: DefaultLayout,
    component: StaffPage
  },{
    path: "/stock",
    layout: DefaultLayout,
    component: Stock
  },
  {
    path: "/money",
    layout: DefaultLayout,
    component: MoneyPage
  },
  // {
  //   path: "/user-profile-lite",
  //   layout: DefaultLayout,
  //   component: UserProfileLite
  // },
  // {
  //   path: "/add-new-post",
  //   layout: DefaultLayout,
  //   component: AddNewPost
  // },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  // {
  //   path: "/components-overview",
  //   layout: DefaultLayout,
  //   component: ComponentsOverview
  // },
  // {
  //   path: "/tables",
  //   layout: DefaultLayout,
  //   component: Tables
  // },
  // {
  //   path: "/blog-posts",
  //   layout: DefaultLayout,
  //   component: BlogPosts
  // }
];

const publicRoutes =  [
  {
    path: "/",
    exact: true,
    layout: AltLayout,
    component: Welcome
  },
  {
    path: "/login",
    exact: true,
    layout: AltLayout,
    component: LoginPage
  }
];

const Routes2 = ({token}) => {
  const privateRoutesList = privateRoutes.map((item, id) => {
    return <AppRoute key={id} exact path={item.path} layout={item.layout} component={item.component} />
  });
  const publicRoutesList = publicRoutes.map((item, id) => {
    return <AppRoute key={id} exact path={item.path} layout={item.layout} component={item.component} />
  });

  return (
  <div>
    <Switch>
      {token ? privateRoutesList : publicRoutesList}
      <AppRoute exact path="*" layout={AltLayout} component={Errors} />
      {/*<Redirect from="*" to="/"/>*/}
    </Switch>
  </div>
)};

export default withRouter(connect(
  (state) => ({
    token: state.admin.token
  }),
  (dispatch) => ({
    getProductGraph: ({
                        token,
                        year,
                        month,
                        store
                      }) => dispatch(getProductGraph({
      token,
      year,
      month,
      store
    })),
  })
)(Routes2));
