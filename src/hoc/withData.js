
import React from 'react';
import {getSingleProjectStart} from "../services/actions";
import scrollTop from "../utils/scrollTop";

const withData = ({ children }) => {
  class WithData extends React.Component {
    componentDidMount() {
      const { getSingleProjectStart, match: { params: { proejctId } } } = this.props;
      scrollTop();
      getSingleProjectStart(proejctId)
    }

    render() {
      const { project, projectLoading, ...otherProps } = this.props;

      return !project ? (
        <h1>LOADING</h1>
      ) : (
        <WrappedComponent data={project} {...otherProps} />
      );
    }
  }

  return WithData;
};

export default withData;
