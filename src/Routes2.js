import React, {Component} from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom"
import {DefaultLayout} from "./layouts";
// import UserProfileLite from "./views/UserProfileLite";
// import AddNewPost from "./views/AddNewPost";
import Errors from "./views/Errors";
// import ComponentsOverview from "./views/ComponentsOverview";
// import Tables from "./views/Tables";
// import BlogPosts from "./views/BlogPosts";
import {connect} from "react-redux";

import BlogOverview from "./views/ProjectsPage/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import AddNewPost from "./views/AddNewPost";
import ComponentsOverview from "./views/ComponentsOverview";
import Tables from "./views/Tables";
import BlogPosts from "./views/BlogPosts";
import AddNewProject from "./views/AddNewProject/AddNewProject";
import Categories from "./views/Categories/Categories";
import ProjectPage from "./views/ProjectPage/ProjectPage";
import NotificationsPage from "./views/NotificationsPage/NotificationsPage";
import {getNotificationListStart} from "./services/actions";
import StatisticsPage from "./views/StatisticsPage/StatisticsPage";

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
);

const AltLayout = props => props.children;

const privateRoutes =  [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/projects" />
  },
  {
    path: "/projects",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/projects/:proejctId",
    layout: DefaultLayout,
    component: ProjectPage
  },
  {
    path: "/add-new-project",
    layout: DefaultLayout,
    component: AddNewProject
  },{
    path: "/statistics",
    layout: DefaultLayout,
    component: StatisticsPage
  },
  {
    path: "/notifications",
    layout: DefaultLayout,
    component: NotificationsPage
  },
  {
    path: "/categories",
    layout: DefaultLayout,
    component: Categories
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  }
];

// const publicRoutes =  [
//   {
//     path: "/",
//     exact: true,
//     layout: AltLayout,
//     component: Welcome
//   },
//   {
//     path: "/login",
//     exact: true,
//     layout: AltLayout,
//     component: LoginPage
//   }
// ];

class Routes2 extends Component {
  componentDidMount() {
    const { notificationPeriod, getNotificationListStart } = this.props;
    getNotificationListStart(notificationPeriod)
  }

  render() {
    const privateRoutesList = privateRoutes.map((item, id) => {
      return <AppRoute key={id} exact path={item.path} layout={item.layout} component={item.component} />
    });
    // const publicRoutesList = publicRoutes.map((item, id) => {
    //   return <AppRoute key={id} exact path={item.path} layout={item.layout} component={item.component} />
    // });

    return (
      <div>
        <Switch>
          {/*{token ? privateRoutesList : publicRoutesList}*/}
          {privateRoutesList}
          <AppRoute exact path="*" layout={AltLayout} component={Errors} />
          {/*<Redirect from="*" to="/"/>*/}
        </Switch>
      </div>)
  }
}

export default withRouter(connect(
  (state) => ({
    // token: state.admin.token
    notificationPeriod: state.notification.period
  }),
  (dispatch) => ({
    getNotificationListStart: (days) => dispatch(getNotificationListStart(days)),
  })
)(Routes2));
