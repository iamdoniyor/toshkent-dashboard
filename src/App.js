import React from "react";
import { Router } from "react-router-dom";
import {PersistGate} from "redux-persist/integration/react";
import {Provider} from "react-redux";

import history from './services/history';
import "bootstrap/dist/css/bootstrap.min.css";
import "./shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import Routes2 from "./Routes2";

import {store , persistor} from "./services/Store";


function App() {
  return (<Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router history={history}>
        <Routes2/>
      </Router>
    </PersistGate>
  </Provider>)
}

export default App;
