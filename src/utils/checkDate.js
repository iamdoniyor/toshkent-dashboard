export const checkDate = d => {
  if (Object.prototype.toString.call(d) === "[object Date]") {
    if (isNaN(d.getTime())) {  // d.valueOf() could also work
      // date is not valid
      return false
    } else {
      // date is valid
      return true
    }
  } else {
    // not a date
    return false
  }
};
